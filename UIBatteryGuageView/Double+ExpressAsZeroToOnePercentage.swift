//
//  Double+ExpressAsZeroToOnePercentage.swift
//
//  Created by Damien Laughton on 31/03/2017.
//  No Rights resereved.
//

import Foundation

extension Double {
  func expressAsZeroToOnePercentage() -> Double {
    var zeroToOneBase = 0.0
    if self < 0.0 {
      zeroToOneBase = 0.0
    } else if self <= 1.0 {
      zeroToOneBase = self
    } else if self <= 100.0 {
      zeroToOneBase = self / 100
    } else {
      zeroToOneBase = 1.0
    }
    
    return zeroToOneBase
  }
}
